/* 
  old_panel.strings
  Enphase

  Created by Javier Daccorso on 2/3/17.
  Copyright © 2017 99UNO. All rights reserved.
*/
//
//  ViewController.swift
//  Enphase
//
//  Created by Javier Daccorso on 17/1/17.
//  Copyright © 2017 99UNO. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private static let TILE_ARRAY_BORDER : CGFloat = 10
    private let TILE_WIDTH : CGFloat = 70
    private let TILE_HEIGHT : CGFloat = 100
    private let TILE_MARGIN : CGFloat = 3
    
    var tile : UIView!
    var activeBorderLayer : CAShapeLayer!
    var activeGroup : UIView!
    var tileArray = [UIView]()
    var groupArray = [UIView]()
    var rotationBegin : CGFloat = 0
    var rotationEnd : CGFloat = 0
    
    @IBOutlet var garbageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.dashedBorderLayerWithColor(view: tileArrayView, color: UIColor.gray.cgColor)
        //        self.tile = createTile(x: 0, y: 0)
        //        self.dashedBorderLayerWithColor(view: self.tile, color: UIColor.gray.cgColor)
    }
    
    func moveGroupWithGestureRecognizer(panGestureRecognizer : UIPanGestureRecognizer) {
        let touchLocation : CGPoint = panGestureRecognizer.location(in: self.view)
        let theView = panGestureRecognizer.view!
        theView.center = touchLocation
    }
    
    func moveViewWithGestureRecognizer(panGestureRecognizer : UIPanGestureRecognizer) {
        
        let touchLocation : CGPoint = panGestureRecognizer.location(in: self.activeGroup)
        let theView = panGestureRecognizer.view!
        theView.center = touchLocation
        
        //        let velocity : CGPoint = panGestureRecognizer.velocity(in: self.view)
        //        updateTileArray()
        
        //        print(String.init(format: "Horizontal Velocity: %.2f points/sec", velocity.x))
        //        print(String.init(format: "Vertical Velocity: %.2f points/sec", velocity.y))
        
        switch panGestureRecognizer.state {
            case UIGestureRecognizerState.began:
            //    self.borderLayer.removeFromSuperlayer()
            break
            case UIGestureRecognizerState.ended, UIGestureRecognizerState.cancelled:
            //  self.dashedBorderLayerWithColor(view: self.tile, color: UIColor.gray.cgColor)
            
            checkCollision()
            updateGroup(view: theView.superview!)
            break
            case UIGestureRecognizerState.changed:
            if (self.garbageView.frame.intersects(theView.frame)) {
                panGestureRecognizer.view!.removeFromSuperview()
                if let index = tileArray.index(of: theView) {
                    tileArray.remove(at: index)
                }
            }
            default:
            print("nada")
        }
    }
    
    func handleRotationWithGestureRecognizer(gesture : UIRotationGestureRecognizer) {
        
        //        print (gesture.state)
        let theView = gesture.view!
        theView.transform = theView.transform.rotated(by: gesture.rotation);
        print(gesture.rotation)
        
        switch gesture.state {
            case UIGestureRecognizerState.began:
            self.rotationBegin = gesture.rotation
            break
            case UIGestureRecognizerState.ended:
            self.rotationEnd = gesture.rotation
            theView.transform = theView.transform.rotated(by: 1);
            break
            case UIGestureRecognizerState.cancelled:
            break
            
            default:
            print("nada")
        }
    }
    
    func checkCollision() {
        
        for view1 : UIView in self.tileArray {
            view1.backgroundColor = UIColor.orange
            for view2 : UIView in self.tileArray {
                if ((view1 != view2) && (view1.frame.intersects(view2.frame))) {
                    view1.backgroundColor = UIColor.red
                    view2.backgroundColor = UIColor.red
                }
            }
        }
    }
    
    func updateGroup(view : UIView) {
        
        var minX : CGFloat = 0
        var minY : CGFloat = 0
        var maxX : CGFloat = 0
        var maxY : CGFloat = 0
        
        for view : UIView in tileArray {
            if (minX == 0 || view.frame.origin.x < minX) {
                minX = view.frame.origin.x
            }
            if (minY == 0 || view.frame.origin.y < minY) {
                minY = view.frame.origin.y
            }
            if (view.frame.size.width > maxX) {
                maxX = view.frame.size.width
            }
            if (view.frame.size.height > maxY) {
                maxY = view.frame.size.height
            }
        }
        
        print(minX)
        print(minY)
        print(maxX)
        print(maxY)
        
        //        self.activeGroup.removeFromSuperview()
        //
        //
        //
        //
        //        let group = UIView(frame: CGRect(
        //            x: self.activeGroup.frame.origin.x + minX - TILE_MARGIN,
        //            y: self.activeGroup.frame.origin.y + minY - TILE_MARGIN,
        //            width: maxX + TILE_MARGIN,
        //            height: maxY + TILE_MARGIN ))
        //        group.backgroundColor = UIColor.clear
        //
        //        let panGestureRecognizer : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveGroupWithGestureRecognizer))
        //        group.addGestureRecognizer(panGestureRecognizer)
        //        group.backgroundColor = UIColor.green
        //        //self.dashedBorderLayerWithColor(view: group, color: UIColor.gray.cgColor)
        //
        //        self.activeGroup = group
        //        groupArray.append(group)
        ////
        //        self.view.addSubview(group)
        //
        //
        //
        ////        self.activeGroup.frame = CGRect(x: self.activeGroup.frame.origin.x + minX, y: self.activeGroup.frame.origin.y + minY, width: minX + maxX + TILE_MARGIN, height: minY + maxY + TILE_MARGIN)
        //
        //
        //
        //        let frame = self.view.convert(self.activeGroup.frame, from:view)
        //        print(frame.origin.x)
        //
        //
        ////        let p = view.convert(view.center, to: self.view)
        ////
        ////        print(p.x)  // this prints the x coordinate of 'obj' relative to the screen
        ////        print(p.y)  // this prints the y coordinate of 'obj' relative to the screen
        
    }
    
    func dashedBorderLayerWithColor(view: UIView, color:CGColor) {
        
        self.activeBorderLayer = CAShapeLayer()
        activeBorderLayer.name  = "borderLayer"
        let frameSize = view.frame.size
        let border = ViewController.TILE_ARRAY_BORDER
        let shapeRect = CGRect(x: -border, y: -border, width: frameSize.width + border, height: frameSize.height + border)
        
        self.activeBorderLayer.bounds=shapeRect
        self.activeBorderLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        self.activeBorderLayer.fillColor = UIColor.clear.cgColor
        self.activeBorderLayer.strokeColor = color
        self.activeBorderLayer.lineWidth=1
        self.activeBorderLayer.lineJoin=kCALineJoinRound
        self.activeBorderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8), NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        self.activeBorderLayer.path = path.cgPath
        
        view.layer.addSublayer(self.activeBorderLayer)
    }
    
    func createTile(x: CGFloat, y: CGFloat) -> UIView {
        let tile = UIView(frame: CGRect(x: x, y: y, width: self.TILE_WIDTH, height: TILE_HEIGHT ))
        tile.backgroundColor = UIColor.orange
        
        let panGestureRecognizer : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveViewWithGestureRecognizer))
        tile.addGestureRecognizer(panGestureRecognizer)
        
        let rotationGestureRecognizer : UIRotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action:#selector(handleRotationWithGestureRecognizer))
        tile.addGestureRecognizer(rotationGestureRecognizer)
        
        return tile
    }
    
    @IBAction func onAddGrupoClick(_ sender: Any) {
        let group = UIView(frame: CGRect(x: 0, y: 0, width: self.TILE_WIDTH, height: TILE_HEIGHT ))
        group.backgroundColor = UIColor.clear
        
        let panGestureRecognizer : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveGroupWithGestureRecognizer))
        group.addGestureRecognizer(panGestureRecognizer)
        group.backgroundColor = UIColor.green
        //self.dashedBorderLayerWithColor(view: group, color: UIColor.gray.cgColor)
        self.activeGroup = group
        groupArray.append(group)
        
        self.view.addSubview(group)
    }
    
    @IBAction func onAddOneClick(_ sender: Any) {
        
        let tile = createTile(x: 0, y: 0)
        tileArray.append(tile)
        //        self.activeGroup.addSubview(tile)
        
        self.view.addSubview(tile)
    }
    
    @IBAction func onAddMatrixClick(_ sender: Any) {
        
        for indexX in 0 ..< 2 {
            for indexY in 0 ..< 2 {
                let posX : CGFloat = CGFloat(indexX) * (TILE_WIDTH + TILE_MARGIN)
                let posY : CGFloat = CGFloat(indexY) * (TILE_HEIGHT + TILE_MARGIN)
                let tile = createTile(x: posX, y: posY)
                tileArray.append(tile)
                self.view.addSubview(tile)
            }
        }
    }
}

