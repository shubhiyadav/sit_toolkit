import os, unittest
from appium import webdriver

class Toolkit(unittest.TestCase):
	def setUp(self):
		desired_caps = {}
		desired_caps['platformName'] = 'iOS'
		desired_caps['platformVersion'] = '11.0'
		desired_caps['deviceName'] = 'iPhone 8'
                desired_caps['udid'] = '223817d56ffb9e1d21015b25167238b0fe38a82e'
                desired_caps['app'] = os.path.join('/Users/syadav/Documents/Appium/toolkit-ios/apps/Enphase.app')
                self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

	def tearDown(self):
		# each test case will start from begining 
		self.driver.quit()

	def test_open_application(self):
		self.assertTrue(self.driver.find_element_by_name("Login"))
	

if __name__ == '__main__':
	suite = unittest.TestLoader().loadTestsFromTestCase(Toolkit)
	unittest.TextTestRunner(verbosity=2).run(suite)

	
