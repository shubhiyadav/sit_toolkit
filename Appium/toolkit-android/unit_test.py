import os
from time import sleep

import unittest

from appium import webdriver

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

class Toolkit(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '8.0.0'
        desired_caps['deviceName'] = 'Android Emulator'
        desired_caps['app'] = PATH('/Users/syadav/Documents/Appium/toolkit-android/apps/app-debug.apk')
        desired_caps['useNewWDA'] = 'true'
        desired_caps['appPackage'] = 'com.enphase.installer_toolkit'
        desired_caps['appActivity'] = '.ui.activity.MainActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)


    def tearDown(self):
        # end the session
        self.driver.quit()

    def test_whether_app_is_installed(self):
        print "Test if the app has been installed correctly"
        self.driver.is_app_installed('com.enphase.installer_toolkit')

        print('APP HAS BEEN INSTALLED')


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Toolkit)
    unittest.TextTestRunner(verbosity=2).run(suite)
